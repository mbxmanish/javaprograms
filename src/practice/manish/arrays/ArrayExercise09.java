package practice.manish.arrays;

import java.util.Arrays;

public class ArrayExercise09 {

	public static void main(String[] args) {
		int[] myArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		System.out.println("Initial array is: " + Arrays.toString(myArray) + "\n");
		insertElement(myArray, 5, 12);

	}

	private static void insertElement(int[] array, int index, int num) {
		System.out.println("Inserting element in the array.............");
		int[] newArray = new int[array.length + 1];
		for (int i = array.length - 1; i >= index; i--) {
			newArray[i + 1] = array[i];
			newArray[index] = num;
		}
		for (int i = 0; i < index; i++) {
			newArray[i] = array[i];
		}

		System.out.println(Arrays.toString(newArray));

	}

}
