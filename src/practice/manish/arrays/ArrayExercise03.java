package practice.manish.arrays;

public class ArrayExercise03 {

	public static void main(String[] args) {

		System.out.println("The Grid pattern is: \n");
		char[][] a = new char[10][10];
		char myEntryVal = '-';

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				a[i][j] = myEntryVal;
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
	}

}
