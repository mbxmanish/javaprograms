package practice.manish.arrays;

public class ArrayExercise05 {

	public static void main(String[] args) {
		int[] myArray = { 4, 3, 2, 1, 2, 6, 7, 9, 8 };
		System.out.println(mySearch(myArray, 5));
	}

	private static String mySearch(int[] a, int numToSearch) {
		for (int element : a) {
			if (numToSearch == element) {
				return "Yes, the value exists in the given array";
			}
		}
		return "No, the value does no exist in the given array";
	}
}
