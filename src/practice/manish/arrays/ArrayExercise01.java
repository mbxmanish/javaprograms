package practice.manish.arrays;
import java.util.Arrays;

public class ArrayExercise01 {

	public static void main(String[] args) {

		int[] array1 = { 4, 3, 2, 1, 2, 6, 7, 9, 8 };
		String[] array2 = { "Manish", "Abhishek", "Deepak", "Anurag", "Aashish" ,"Aaaashish"};

		// Sorting Integer using Arrays.sort method()
		// Arrays.sort(array1);
		// System.out.println("The initial array is: " + Arrays.toString(array1));

		// Sorting String array using Arrays.sort method()
		// Arrays.sort(array2);
		// System.out.println("The Sorted array is: " + Arrays.toString(array2));

		System.out.println("Final Sorted Array is:  " + Arrays.toString(mySort(array1)));
		System.out.println("Final Sorted Array is:  " + Arrays.toString(mySort(array2)));
	}

	public static int[] mySort(int[] a) {
		int temp;
		System.out.println("***************************************************************************"+"\n");
		System.out.println("The unsorted array is: " + Arrays.toString(a)+"\n"+"\n");
		for (int i = 0; i < a.length - 1; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] > a[j]) {
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
					System.out.println("  Value of i: " + i + " & Value of j:" + j);
					System.out.println("  Current sorted Array is:" + Arrays.toString(a)+"\n");
				}
			}
		}
		// System.out.println("The sorted array is: " + Arrays.toString(a));
		return a;
	}
	
	public static String[] mySort(String[] a) {
		String temp;
		System.out.println("***************************************************************************"+"\n");
		System.out.println("The unsorted array is: " + Arrays.toString(a)+"\n"+"\n");
		for (int i = 0; i < a.length - 1; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if ((a[i].compareTo(a[j]))>0) {
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
					System.out.println("  Value of i: " + i + " & Value of j:" + j);
					System.out.println("  Current sorted Array is: " + Arrays.toString(a)+"\n");
				}
			}
		}
		// System.out.println("The sorted array is: " + Arrays.toString(b));
		return a;
	}
	
	

}