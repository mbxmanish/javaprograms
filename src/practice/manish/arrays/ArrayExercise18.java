package practice.manish.arrays;

import java.util.Arrays;

public class ArrayExercise18 {

	public static void main(String[] args) {
		int[] array = { 123, 232, 12334, 23, 21 };
		System.out.println("Initial Array is: " + Arrays.toString(array) + "\n");
		secondSmallestElement(array);
	}

	private static void secondSmallestElement(int[] myArray) {
		System.out.println("Finding the second smallest element in given array.......");
		Arrays.sort(myArray);
		System.out.println(myArray[1]);

	}
}