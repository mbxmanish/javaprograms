package practice.manish.arrays;

public class ArrayExercise06 {

	public static void main(String[] args) {
		int[] myArray = { 4, 3, 2, 1, 2, 6, 7, 9, 11 };
		myIndexFinder(myArray, 2);
	}

	private static void myIndexFinder(int[] a, int num) {

		for (int i = 0; i < a.length; i++) {
			if (a[i] == num) {
				System.out.println("The index of given number is : " + i);
				break;
			} else if (a[i] != num && i == a.length - 1) {
				System.out.println("Number not found in the given Array");
			}
		}
	}
}