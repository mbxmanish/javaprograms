package practice.manish.arrays;

import java.util.Arrays;

public class ArrayExercise10 {

	public static void main(String[] args) {
		int[] array = { 123, 232, 12334, 23, 21 };
		System.out.println("Initial Array is: " + Arrays.toString(array) + "\n");
		maxElement(array);
		minElement(array);

	}

	private static void minElement(int[] myArray) {
		System.out.println("Finding the min element in given array.......");
		Arrays.sort(myArray);
		System.out.println(myArray[0]);

	}

	private static void maxElement(int[] myArray) {
		;
		System.out.println("Finding the max element in given array.......");
		Arrays.sort(myArray);
		System.out.println(myArray[myArray.length - 1]);
	}

}
