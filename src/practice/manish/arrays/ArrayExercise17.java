package practice.manish.arrays;

import java.util.Arrays;

public class ArrayExercise17 {

	public static void main(String[] args) {
		int[] array = { 123, 232, 12334, 23, 21 };
		System.out.println("Initial Array is: " + Arrays.toString(array) + "\n");
		secondLargestElement(array);
	}

	private static void secondLargestElement(int[] myArray) {

		System.out.println("Finding the second largest element in given array.......");
		Arrays.sort(myArray);
		System.out.println(myArray[myArray.length - 2]);
	}

}
