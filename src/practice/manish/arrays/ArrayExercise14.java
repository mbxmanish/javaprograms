package practice.manish.arrays;

import java.util.Arrays;

public class ArrayExercise14 {
	public static void main(String[] args) {
		String[] myArray1 = { "Manish", "Deepak", "Manish", "Abhishek", "Anurag", "Manish" ,"Rocky"};
		String[] myArray2 = { "ABC", "XYZ", "Manish", "Abhishek", "Deepak", "Manish", "Anurag", "Rocky" };
		commonElementChecker(myArray1, myArray2);
	}

	private static void commonElementChecker(String[] array1, String[] array2) {
		String[] commonElements = new String[array1.length];
		System.out.println("Common Elements from given arrays:" + "\n");
		
		for (int i = 0; i <= array1.length - 1; i++) {
			for (int j = 0; j <= array2.length - 1; j++) {
				if (array1[i] == array2[j]) {
					if (Arrays.asList(commonElements).contains(array1[i])) {
					} else {
						commonElements[i] = array1[i];
						System.out.println(commonElements[i]);
					}
				}
			}
		}
	}
}
