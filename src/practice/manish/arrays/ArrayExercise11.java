package practice.manish.arrays;

import java.util.Arrays;

public class ArrayExercise11 {

	public static void main(String[] args) {
		int[] myArray = { 11, 2, 3, 4, 50, 6, 7, 8, 91 };
		myReverse(myArray);
	}

	private static void myReverse(int[] array) {

		int newIndex = array.length - 1;
		int[] reversedArray = new int[newIndex + 1];
		for (int i = 0; i <= array.length - 1; i++) {
			reversedArray[newIndex - i] = array[i];
		}
		System.out.println(Arrays.toString(reversedArray));
	}

}