package practice.manish.arrays;

import java.util.Arrays;

public class ArrayExercise04 {

	public static void main(String[] args) {
		int[] array1 = { 4, 3, 2, 1, 2, 6, 7, 9, 8 };
		System.out.println("The array is: " + Arrays.toString(array1)+"\n");
		System.out.println("The average value of elements in array is: " + myAvg(array1));

	}

	public static int myAvg(int[] a) {
		int sum = 0, avg;
		
		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		System.out.println("Sum of all elements in the array is : " +sum);
		System.out.println("Number of ELements in the array are: "+a.length+"\n");
		avg = sum/(a.length);
		return avg;
	}

}
