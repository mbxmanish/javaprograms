package practice.manish.arrays;

public class ArrayExercise12 {

	public static void main(String[] args) {
		int[] myArray = { 11, 33, 11, 33, 4, 50, 6, 7, 8, 91, 11, 11 };
		myDupChecker(myArray, 11);

	}

	private static void myDupChecker(int[] array, int num) {
		int numOfOccurences = 0;
		for (int i = 0; i < array.length; i++) {
			if (num == array[i]) {
				numOfOccurences++;
			} else {
				System.out.println("Searching......");
			}
		}

		System.out.println("This element was found " + numOfOccurences + " times.");
	}
}
