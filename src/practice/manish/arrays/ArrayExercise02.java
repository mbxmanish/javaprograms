package practice.manish.arrays;

import java.util.Arrays;

public class ArrayExercise02 {

	public static void main(String[] args) {
		int[] array1 = { 4, 3, 2, 1, 2, 6, 7, 9, 8 };
		System.out.println("The array is: " + Arrays.toString(array1));
		System.out.println("The sum of elements in array is: " + mySum(array1));

	}

	public static int mySum(int[] a) {
		int sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		return sum;
	}
}
