package practice.manish.arrays;

public class ArrayExercise13 {

	public static void main(String[] args) {
		String[] myArray = { "Manish", "Deepak", "Manish", "Abhishek", "Anurag", "Manish" };
		myDupChecker(myArray, "Manish");
	}

	private static void myDupChecker(String[] array, String str) {
		int numOfOccurences = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i].compareTo(str) == 0) {
				numOfOccurences++;
			} else {
				System.out.println("Searching......");
			}
		}
		System.out.println("This element was found " + numOfOccurences + " times.");
	}
}