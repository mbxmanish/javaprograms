package practice.manish.arrays;

public class ArrayExercise15 {

	public static void main(String[] args) {

		int[] myArray1 = { 1, 2, 3, 4, 5, 6 };
		int[] myArray2 = { 4, 5, 6, 7, 8, 9 };
		commonElementChecker(myArray1, myArray2);
	}

	private static void commonElementChecker(int[] array1, int[] array2) {
		int[] commonElements = new int[array1.length];
		System.out.println("Common Elements from given arrays:" + "\n");

		for (int i = 0; i <= array1.length - 1; i++) {
			for (int j = 0; j <= array2.length - 1; j++) {
				if (array1[i] == array2[j]) {
					commonElements[i] = array1[i];
					System.out.println(commonElements[i]);
				}
			}
		}
	}
}
