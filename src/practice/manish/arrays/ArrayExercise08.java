package practice.manish.arrays;

import java.util.Arrays;

public class ArrayExercise08 {

	public static void main(String[] args) {
		int array[] = { 1, 2, 3, 4, 5, 5, 6, 7, 8 };
		String array1[] = { "ABC", "XYZ", "JKL", "MNOP" };
		System.out.println(Arrays.toString(array));
		myArrayCopier(array);
		System.out.println(Arrays.toString(array1));

		myArrayCopier(array1);

	}

	private static void myArrayCopier(String[] myArray1) {
		String copiedArray[] = new String[myArray1.length];
		// System.out.println(Arrays.toString(copiedArray));
		for (int i = 0; i <= myArray1.length - 1; i++) {
			copiedArray[i] = myArray1[i];
			System.out.println("Copying array by iterating element # " + i + " of the array...." + "\n"
					+ Arrays.deepToString(copiedArray));
		}
		System.out.println("\n");
	}

	private static void myArrayCopier(int[] myArray) {
		int copiedArray[] = new int[myArray.length];
		// System.out.println(Arrays.toString(copiedArray));
		for (int i = 0; i <= myArray.length - 1; i++) {
			copiedArray[i] = myArray[i];
			System.out.println("Copying array by iterating element # " + i + " of the array...." + "\n"
					+ Arrays.toString(copiedArray));
		}
		System.out.println("\n");

	}

}
