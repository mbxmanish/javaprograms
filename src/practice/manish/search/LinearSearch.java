package practice.manish.search;

public class LinearSearch {

	public static void main(String[] args) {
		int[] myArray = { 1, 2, 3, 4, 5, 5, 67, 8, 8, 9 };
		int numToFind = 989;

		LinearSearch ls = new LinearSearch();
		System.out.println("Number " + numToFind + " was found at index = " + ls.searchLinear(myArray, numToFind));

	}

	private int searchLinear(int[] array, int num) {
		for (int i = 0; i <= array.length - 1; i++) {
			if (array[i] == num) {
				return i;
			}
		}
		return -1;
	}
}
